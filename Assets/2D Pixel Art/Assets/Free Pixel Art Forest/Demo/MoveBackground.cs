﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour
{
	public bool isPlayerMoving = false;
	public float speed;
	private float x;
	public float PontoDeDestino;
	public float PontoOriginal;

	void Update ()
	{
		if (!isPlayerMoving)
			return;


		x = transform.position.x;
		x += speed * Time.deltaTime;
		transform.position = new Vector3 (x, transform.position.y, transform.position.z);

		if (x <= PontoDeDestino)
		{
			x = PontoOriginal;
			transform.position = new Vector3(x, transform.position.y, transform.position.z);
		}
	}
}
