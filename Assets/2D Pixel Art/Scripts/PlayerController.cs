using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D rb;

    [Header("Movement")]
    [SerializeField] private bool canMove = true;
    [SerializeField] private float speed;
    [SerializeField] private bool isRolling = false;

    [Header("Ground Check")]
    [SerializeField] private bool isGrounded = false;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private float groundRadius;

    [Header("Jump")]
    [SerializeField] private bool isJumping;
    [SerializeField] private float jumpForce;

    [Header("Combat")]
    [SerializeField] private bool isAttacking = false;
    [SerializeField] private bool isBlocking = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Roll();
        Jump();
        Combat();
    }

    private void FixedUpdate()
    {
        Move();
        GroundCheck();
    }

    private void Move()
    {
        if (!canMove)
            return;

        float moveX = Input.GetAxis("Horizontal");

        transform.position += new Vector3(moveX, 0f, 0f) * Time.deltaTime * speed;

        anim.SetFloat("speed", Mathf.Abs(moveX));

        if (!Mathf.Approximately(0, moveX))
        {
            transform.rotation = moveX < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
        }

        anim.SetBool("isJumping", !isGrounded);
    }

    private void Roll()
    {
        if (!isRolling && Input.GetKeyDown(KeyCode.LeftShift))
        {
            anim.SetTrigger("roll");
            isRolling = true;
            speed += 5f;
        }
    }
    private void EndRoll()
    {
        isRolling = false;
        speed -= 5f;
    }

    private void Jump()
    {
        if (isGrounded && !isJumping && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

    private void GroundCheck()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, layerMask);
    }

    private void Combat()
    {
        if (isGrounded && !isJumping && !isRolling && Input.GetMouseButton(0))
        {
            canMove = false;
            isAttacking = true;
            anim.SetBool("isAttacking", isAttacking);
        }
        else
        {
            canMove = true;
            isAttacking = false;
            anim.SetBool("isAttacking", isAttacking);
        }

        if (isGrounded && !isJumping && !isRolling && Input.GetMouseButton(1))
        {
            canMove = false;
            isBlocking = true;
            anim.SetBool("isBlocking", isBlocking);
        }
        else
        {
            canMove = true;
            isBlocking = false;
            anim.SetBool("isBlocking", isBlocking);
        }
    }
}
