using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private float _moveSpeed;
    [SerializeField] private bool _canMove = true;
    [SerializeField] private bool isRolling = false;

    [Header("Ground Check")]
    [SerializeField] private bool isGrounded = false;
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask layerMask;
    [SerializeField] private float groundRadius;

    [Header("Jumping")]
    [SerializeField] private float jumpForce = 5f;

    private Animator anim;
    private Rigidbody2D rb;

    [Header("Combat")]
    [SerializeField] private bool isAttacking = false;
    public bool isBlocking = false;
    [SerializeField] private Sword sword;

    [Header("Controls")]
    [SerializeField] private KeyCode jumpKey;
    [SerializeField] private KeyCode rollKey;

    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
    }



    private void Update()
    {
        Jump();
        Roll();
        Combat();
    }

    private void FixedUpdate()
    {
        Move();
        GroundCheck();
    }

    void Jump()
    {
        if (isGrounded && Input.GetKeyDown(jumpKey))
        {
            rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
        }
    }

    void Roll()
    {
        if (!isBlocking && !isAttacking && !isRolling && Input.GetKeyDown(rollKey))
        {
            anim.SetTrigger("roll");
            _moveSpeed += 2.5f;
            isRolling = true;

            GetComponent<CapsuleCollider2D>().enabled = false;
            GetComponentInChildren<BoxCollider2D>().enabled = true;
        }
    }
    public void EndRoll()
    {
        if (!isRolling)
            return;

        _moveSpeed -= 2.5f;

        GetComponent<CapsuleCollider2D>().enabled = true;
        GetComponentInChildren<BoxCollider2D>().enabled = false;

        isRolling = false;
    }

    void Combat()
    {
        if (!isGrounded || isRolling)
            return;

        if (Input.GetMouseButton(0)) // atakowanie
        {
            _canMove = false;
            isAttacking = true;
            anim.SetBool("isAttacking", isAttacking);
        }
        else if (Input.GetMouseButton(1)) // blokowanie
        {
            // tu dodamy blokowanie
            _canMove = false;
            isBlocking = true;
            anim.SetBool("isBlocking", isBlocking);
        }
        else // default
        {
            _canMove = true;
            isAttacking = false;
            isBlocking = false;
            anim.SetBool("isAttacking", isAttacking);
            anim.SetBool("isBlocking", isBlocking);
        }
    }

    void Move()
    {
        if (!_canMove)
            return;

        float moveX = Input.GetAxis("Horizontal");

        anim.SetFloat("move", Mathf.Abs(moveX));

        if (!Mathf.Approximately(0f, moveX))
        {
            transform.rotation = moveX < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
        }

        transform.position += new Vector3(moveX, 0f, 0f) * _moveSpeed * Time.deltaTime;
    }

    private void GroundCheck()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, layerMask);

        anim.SetBool("isJumping", !isGrounded);
    }


    public void Damage()
    {
        if (sword.enemy != null)
        {
            sword.enemy.GetComponent<Health>().GetHit(sword.damage);
        }
    }
    private void OnDrawGizmosSelected() // nie trzeba pisa�
    {
        Gizmos.DrawSphere(groundCheck.position, groundRadius);
    }
}
