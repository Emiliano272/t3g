using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : MonoBehaviour
{
    [Header("State")]
    [SerializeField] private BanditState currentState;

    [Header("Stats")]
    public bool isAlive = true;
    [SerializeField] private float moveSpeed = 2.5f;
    [SerializeField] private float damage = 1f;

    private Animator anim;

    [Header("SI")]
    [SerializeField] private Transform target;
    public float distanceToPlayer;
    [SerializeField] private float stopDistance;

    public bool isTargetInReach = false;
    public float reactionDistance;

    private void Start()
    {
        anim = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        currentState = BanditState.Waiting;
    }

    private void Update()
    {
        switch (currentState)
        {
            case BanditState.Waiting:
                {
                    if (!isAlive)
                        return;
                    if (isTargetInReach)
                        currentState = BanditState.Attacking;

                    anim.SetBool("move", false);
                    anim.SetBool("isAttacking", false);
                    break;
                }
            case BanditState.Attacking:
                {
                    if (!isAlive)
                        return;
                    if (!isTargetInReach)
                        currentState = BanditState.Waiting;

                    distanceToPlayer = (transform.position - target.transform.position).sqrMagnitude;
                    
                    if (target != null && distanceToPlayer > stopDistance)
                    {
                        FlipCharacter();
                        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed * Time.deltaTime);
                        anim.SetBool("move", true);
                        anim.SetBool("isAttacking", false);
                    }
                    else if (target != null && distanceToPlayer < stopDistance)
                    {
                        anim.SetBool("move", false);
                        anim.SetBool("isAttacking", true);
                    }
                    
                    break;
                }
        }
    }

    public void TakeDamage()
    {
        if (target != null && distanceToPlayer < stopDistance)
        {
            target.GetComponent<Health>().GetHit(damage);
            target.GetComponent<PlayerMovement>().EndRoll();
        }
    }
    private void FlipCharacter()
    {
        float directionScale = Mathf.Sign(target.transform.position.x - transform.position.x);
        if (directionScale == 1)
        {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }
}
public enum BanditState
{
    Waiting,
    Attacking
}
