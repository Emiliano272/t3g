using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private float currentHealth;

    [SerializeField] private int indexOfHits = 0;
    [SerializeField] private bool shield = false;

    [Header("Canvas")]
    public Image hpImage;


    private Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
        currentHealth = maxHealth;
    }
    

    public void GetHit(float amount)
    {
        if (GetComponent<PlayerMovement>() && GetComponent<PlayerMovement>().isBlocking)
        {
            anim.SetTrigger("Block");
            return;
        }

        indexOfHits++;
        currentHealth -= amount;
        hpImage.fillAmount = currentHealth / maxHealth;

        if (indexOfHits == 3)
        {
            shield = true;
            Invoke("Shield", .5f);
            indexOfHits = 0;
        }
        if (!shield)
        {
            anim.SetTrigger("Hurt");
        }

        if (currentHealth <= 0f)
        {
            if (GetComponent<Enemy>())
            {
                GetComponent<Enemy>().isAlive = false;
            }
            anim.SetTrigger("Death");
            Destroy(this.gameObject, 1f);
        }

    }
    public void Shield()
    {
        indexOfHits = 0;
        shield = false;
    }
}
