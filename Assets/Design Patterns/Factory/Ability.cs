using UnityEngine;
using UnityEngine.UI;

namespace SimpleFactory
{
    public abstract class Ability
    {
        public abstract string Name { get; }
        public abstract void Process(int amount);
    }
    public class StartFireAbility : Ability
    {
        public override string Name => "Fire";
        public override void Process(int amount)
        {
            var canvas = GameObject.FindGameObjectWithTag("Main Canvas");
            canvas.GetComponentInChildren<Text>().text = "Ability: " + Name + " value " + amount;
        }
    }
    public class HealSelftAbility : Ability
    {
        public override string Name => "Heal";
        public override void Process(int amount)
        {
            var canvas = GameObject.FindGameObjectWithTag("Main Canvas");
            canvas.GetComponentInChildren<Text>().text = "Ability: " + Name + " value " + amount;
        }
    }

    public class DamageAbility : Ability
    {
        public override string Name => "Damage";

        public override void Process(int amount)
        {
            var canvas = GameObject.FindGameObjectWithTag("Main Canvas");
            canvas.GetComponentInChildren<Text>().text = "Ability: " + Name + " value " + amount;
        }
    }

    public static class AbilityFactory
    {
        public static Ability GetAbility(string abilityType)
        {      
            
            switch (abilityType)
            {
                case "Fire":
                    return new StartFireAbility();
                case "Heal":
                    return new HealSelftAbility();
                case "Damage":
                    return new DamageAbility();
                default:
                    return null;
            }
        }
    }
}