using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleFactory;
public class Player : MonoBehaviour
{
    public int meleeDamage;
    public int spellDamage;
    public int heal;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AbilityFactory.GetAbility("Damage").Process(meleeDamage);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            AbilityFactory.GetAbility("Fire").Process(spellDamage);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            AbilityFactory.GetAbility("Heal").Process(heal);
        }
    }
}
