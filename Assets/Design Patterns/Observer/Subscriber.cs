using UnityEngine;

public class Subscriber : MonoBehaviour
{
    private void OnEnable()
    {
        Observer.OnUserAddPoints += OnUserScore;
    }

    private void OnDisable()
    {
        Observer.OnUserAddPoints -= OnUserScore;
    }
    
    private void OnUserScore()
    {
        Debug.Log("On update score");
    }
}
