using UnityEngine;
using System;
public class Observer : MonoBehaviour
{
    public static event Action OnUserAddPoints;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            OnUserAddPoints?.Invoke();
        }
    }
}
