
public abstract class BaseState
{
    public StateMachine owner;

    public virtual void PrepereState()
    {
        // Start
    }
    public virtual void UpdateState()
    {
        // Update
    }
    public virtual void DestroyState()
    {
        // OnDestroy
    }
}
