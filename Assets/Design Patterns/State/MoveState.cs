using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : BaseState
{
    private Vector2 targetPos; // pozycja celu
   
    public override void PrepereState()
    {
        base.PrepereState();

        // Losowanie celu
        targetPos = new Vector2(Random.Range(-8f, 8f), Random.Range(-5f, 5f));
    }

    public override void UpdateState()
    {
        base.UpdateState();

        // Obliczanie kierunku, w kt�rym obiekt ma si� porusza�
        var direction = targetPos - new Vector2(owner.transform.position.x, owner.transform.position.y);

        if (direction.magnitude > 1f)
        {
            direction.Normalize();
        }

        // Podajemy kierunek jako parametr metody Move
        owner.Movement.Move(direction);

        if (direction.magnitude < 0.2f)
        {
            owner.ChangeState(new WaitState());
        }
    }
}
