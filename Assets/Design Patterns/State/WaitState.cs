using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitState : BaseState
{
    public float minWait = 1f; // minimalny czas czekania
    public float maxWait = 3f; // maksymalny czas czekania
    private float waitTime; // ile czasu zosta�o

    public override void PrepereState()
    {
        base.PrepereState();

        // Losowanie czasu
        waitTime = Random.Range(minWait, 2.5f);
    }

    public override void UpdateState()
    {
        base.UpdateState();

        waitTime -= Time.deltaTime; // czasomierz

        if (waitTime <= 0f) // je�eli czas dobije do 0 to zmie� stan
        {
            owner.ChangeState(new MoveState());
        }
    }
}
