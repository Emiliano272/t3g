using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public BaseState _currentState;

    [SerializeField] private SimpleMovement movement;

    public SimpleMovement Movement => movement;

    private void Start()
    {
        var waitState = new WaitState();
        waitState.minWait = 2f;

        ChangeState(waitState);
        Debug.Log("Start state: " + waitState);
    }

    private void Update()
    {
        if (_currentState != null)
        {
            _currentState.UpdateState();
        }
    }
    public void ChangeState(BaseState newState)
    {  
        if (_currentState != null)
            _currentState.DestroyState();

        _currentState = newState;
        Debug.Log("New state: " + newState);

        if (_currentState != null)
        {
            _currentState.owner = this;
            _currentState.PrepereState();
        }
    }
}
