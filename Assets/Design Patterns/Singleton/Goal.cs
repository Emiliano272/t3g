using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public int scoreToAdd;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            GameController.Instance.AddScore(scoreToAdd);
        }
    }
}
